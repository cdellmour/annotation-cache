/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe.setter;

import com.esotericsoftware.reflectasm.FieldAccess;

import java.lang.reflect.Field;

public class FieldSetterCall<DERIVED, PROPERTY_TYPE> implements SetterCall<DERIVED, PROPERTY_TYPE> {
    private final int fieldIndex;
    private final FieldAccess fieldAccess;

    public FieldSetterCall(final Field field) {
        fieldAccess = FieldAccess.get(field.getDeclaringClass());
        fieldIndex = fieldAccess.getIndex(field.getName());
    }

    @Override
    public SetterResult set(final DERIVED instance, final PROPERTY_TYPE value) {
        try {
            fieldAccess.set(instance, fieldIndex, value);
            return SetterResult.success();
        } catch (ClassCastException e) {
            return SetterResult.failed();
        }
    }
}
