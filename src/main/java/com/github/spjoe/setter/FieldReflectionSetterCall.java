/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe.setter;

import java.lang.reflect.Field;

public class FieldReflectionSetterCall<DERIVED, PROPERTY_TYPE> implements SetterCall<DERIVED, PROPERTY_TYPE> {
    private final Field field;

    public FieldReflectionSetterCall(final Field field) {
        this.field = field;
        field.setAccessible(true);
    }

    @Override
    public SetterResult set(DERIVED instance, PROPERTY_TYPE value) {
        SetterResult retVal;

        try {
            field.set(instance, value);
            retVal = SetterResult.success();
        } catch (IllegalArgumentException | IllegalAccessException e) {
            retVal = SetterResult.failed();
        }

        return retVal;
    }
}
