/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe.setter;

import java.util.function.Supplier;

public final class SetterResult {
    private static final SetterResult NO_SETTER = new SetterResult(false, false);
    private static final SetterResult HAS_SETTER_AND_SUCCESS = new SetterResult(true, true);
    private static final SetterResult HAS_SETTER_AND_NO_SUCCESS = new SetterResult(true, false);
    private final boolean setterExist;
    private final boolean success;

    private SetterResult(final boolean doesSetterExist, final boolean success ) {
        this.setterExist = doesSetterExist;
        this.success = success;
    }

    public boolean doesSetterExist() {
        return setterExist;
    }

    public boolean isSuccess() {
        return success;
    }

    public <X extends Throwable> void orElseThrow(final Supplier<X> throwableSupplier) throws X {
        if (!setterExist || !success) {
            throw throwableSupplier.get();
        }
    }

    public static SetterResult noSetter() {
        return NO_SETTER;
    }

    public static SetterResult success() {
        return HAS_SETTER_AND_SUCCESS;
    }

    public static SetterResult failed() {
        return HAS_SETTER_AND_NO_SUCCESS;
    }
}
