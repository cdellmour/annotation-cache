/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe.getter;

import java.util.function.Supplier;

public final class GetterResult<T> {
    private static final GetterResult NO_GETTER_RESULT = new GetterResult(null, false, null);
    private final T result;
    private final boolean getterExist;
    private final Throwable invocationException;

    private GetterResult(final T result, final boolean getterExist, final Throwable invocationException) {
        this.result = result;
        this.getterExist = getterExist;
        this.invocationException = invocationException;
    }

    public boolean doesGetterExist() {
        return getterExist;
    }

    public boolean isSuccess() {
        return getterExist && invocationException == null;
    }

    public T orElse(final T defaultValue) {
        return orElseGet(() -> defaultValue);
    }

    public T orElseGet(final Supplier<T> defaultSupplier) {
        return isSuccess() ? result : defaultSupplier.get();
    }

    public <X extends Throwable> T orElseThrow(final Supplier<X> throwableSupplier) throws X {
        if (isSuccess()) {
            return result;
        } else {
            throw throwableSupplier.get();
        }
    }

    public T get() {
        return orElseThrow(() -> !getterExist
                ? new IllegalStateException("no getter exist to get a result")
                : new IllegalStateException("invocation of getter failed", invocationException));
    }

    static <T> GetterResult<T> noGetter() {
        return NO_GETTER_RESULT;
    }

    static <T> GetterResult<T> failed(final Throwable throwable) {
        return new GetterResult<>(null, true, throwable);
    }

    static <T> GetterResult<T> success(final T result) {
        return new GetterResult<>(result, true, null);
    }
}
