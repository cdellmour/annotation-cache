/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe.getter;

import com.esotericsoftware.reflectasm.MethodAccess;

import java.lang.reflect.Method;

public class MethodGetterCall<DERIVED, PROPERTY_TYPE> implements GetterCall<DERIVED, PROPERTY_TYPE> {
    private final int methodIndex;
    private final MethodAccess methodAccess;

    public MethodGetterCall(final Method method) {
        methodAccess = MethodAccess.get(method.getDeclaringClass());
        methodIndex = methodAccess.getIndex(method.getName());
    }

    @Override
    public GetterResult<PROPERTY_TYPE>  get(final DERIVED instance) {
        return GetterResult.success((PROPERTY_TYPE) methodAccess.invoke(instance, methodIndex));
    }
}
