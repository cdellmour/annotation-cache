/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package com.github.spjoe;

import com.github.spjoe.getter.*;
import com.github.spjoe.setter.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class BeanPropertyAnnotationCache<BASE, A extends Annotation> {
    private final Map<Class<? extends BASE>, Optional<A>> annotations = new ConcurrentHashMap();
    private final Map<Class<? extends BASE>, GetterCall<BASE, Object>> getters = new ConcurrentHashMap();
    private final Map<Class<? extends BASE>, SetterCall<BASE, Object>> setters = new ConcurrentHashMap();
    private final Class<A> annotationClass;

    public BeanPropertyAnnotationCache(final Class<A> annotationClass) {
        this.annotationClass = annotationClass;
    }

    public <DERIVED extends BASE> Optional<A> getAnnotation(final Class<DERIVED> derivedType) {
        return annotations.computeIfAbsent(derivedType, this::createOptionalAnnotation);
    }

    public <DERIVED extends BASE> Optional<A> getAnnotation(final DERIVED instance) {
        return getAnnotation((Class<DERIVED>) instance.getClass());
    }

    public <PROPERTY_TYPE> GetterResult<PROPERTY_TYPE> get(final BASE instance) {
        return (GetterResult<PROPERTY_TYPE>) getters.computeIfAbsent((Class<BASE>) instance.getClass(), this::createGetter).get(instance);
    }

    public <DERIVED extends BASE, PROPERTY_TYPE> SetterResult set(final DERIVED instance, final PROPERTY_TYPE propertyValue ) {
        return setters.computeIfAbsent((Class<DERIVED>) instance.getClass(), this::createSetter).set(instance, propertyValue);
    }

    private <DERIVED extends BASE, PROPERTY_TYPE> SetterCall<BASE, PROPERTY_TYPE> createSetter(final Class<DERIVED> derivedType) {
        return firstOptional(() -> getGetterWithAnnotation(derivedType, annotationClass).findAny().flatMap(getter -> setterOfGetter(getter, derivedType))
                                                    .map(MethodSetterCall<BASE, PROPERTY_TYPE>::new),
                             () -> getFieldsWithAnnotation(derivedType, annotationClass).findAny().map(this::<PROPERTY_TYPE>createFieldSetter))
                .orElseGet(NoSetterCall::new);
    }

    private <DERIVED extends BASE, PROPERTY_TYPE> GetterCall<BASE, PROPERTY_TYPE> createGetter(final Class<DERIVED> derivedType) {
        return firstOptional(() -> getGetterWithAnnotation(derivedType, annotationClass).findAny().map(MethodGetterCall<BASE, PROPERTY_TYPE>::new),
                             () -> getFieldsWithAnnotation(derivedType, annotationClass).findAny().map(this::<PROPERTY_TYPE>createFieldGetter))
                .orElseGet(NoGetterCall::new);
    }

    private <PROPERTY_TYPE> GetterCall<BASE, PROPERTY_TYPE> createFieldGetter(final Field field) {
        GetterCall<BASE, PROPERTY_TYPE> retVal;
        try {
            retVal = new FieldGetterCall<>(field);
        } catch (IllegalArgumentException e) {
            retVal = new FieldReflectionGetterCall<>(field);
        }

        return retVal;
    }

    private <PROPERTY_TYPE> SetterCall<BASE, PROPERTY_TYPE> createFieldSetter(final Field field) {
        SetterCall<BASE, PROPERTY_TYPE> retVal;
        try {
            retVal = new FieldSetterCall<>(field);
        } catch (IllegalArgumentException e) {
            retVal = new FieldReflectionSetterCall<>(field);
        }

        return retVal;
    }

    private <DERIVED extends BASE> Optional<A> createOptionalAnnotation(Class<DERIVED> derivedType) {
        return getGetterWithAnnotation(derivedType, annotationClass).findAny().map(m -> m.getAnnotation(annotationClass)).map(Optional::of)
                .orElseGet(() -> getFieldsWithAnnotation(derivedType, annotationClass).findAny().map(f -> f.getAnnotation(annotationClass)));
    }

    private <DERIVED extends BASE> Optional<Method> setterOfGetter(final Method getter, final Class<DERIVED> derivedClass) {
        String setterName = convertToSetterName(getter.getName());
        return getMethodWithName(derivedClass, setterName);
    }

    private static String convertToSetterName(final String getterName) {
        int nameIndex = getterName.startsWith("is") ? 2 : 3;
        return "set" + getterName.substring(nameIndex);
    }

    private static Optional<Method> getMethodWithName(final Class<?> clazz, final String name) {
        return getAccessibleMethods(clazz).filter(m -> m.getName().equals(name)).findAny();
    }

    private static Stream<Method> getGetterWithAnnotation(final Class<?> clazz, final Class<? extends Annotation> annotationClass) {
        return getGetterMethods(clazz).filter(m -> m.isAnnotationPresent(annotationClass));
    }

    private static Stream<Field> getFieldsWithAnnotation(final Class<?> clazz, final Class<? extends Annotation> annotationClass) {
        return getAccessibleFields(clazz).filter(f -> f.isAnnotationPresent(annotationClass));
    }

    private static Stream<Method> getGetterMethods(final Class<?> clazz) {
        return getAccessibleMethods(clazz).filter(m -> m.getName().startsWith("get") || m.getName().startsWith("is"));
    }

    private static Stream<Method> getAccessibleMethods(final Class<?> clazz) {
        Iterable<Class<?>> iterable = () -> new SuperClassIterator(clazz);
        Stream<Class<?>> classHierarchy = StreamSupport.stream(iterable.spliterator(), false);
        return classHierarchy.flatMap(c -> Stream.of(c.getDeclaredMethods()));
    }

    private static Stream<Field> getAccessibleFields(final Class<?> clazz) {
        Iterable<Class<?>> iterable = () -> new SuperClassIterator(clazz);
        Stream<Class<?>> classHierarchy = StreamSupport.stream(iterable.spliterator(), false);
        return classHierarchy.flatMap(c -> Stream.of(c.getDeclaredFields()));
    }

    private static class SuperClassIterator implements Iterator<Class<?>> {
        private Class clazz;

        public SuperClassIterator(final Class clazz) {
            this.clazz = clazz;
        }

        @Override
        public boolean hasNext() {
            return clazz != null;
        }

        @Override
        public Class next() {
            Class retVal = clazz;
            clazz = clazz.getSuperclass();
            return retVal;
        }
    }

    private static <T> Optional<T> firstOptional(final Supplier<Optional<T>>... suppliers) {
        return Arrays.asList(suppliers).stream().map(Supplier::get).flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty)).findFirst();
    }
}
