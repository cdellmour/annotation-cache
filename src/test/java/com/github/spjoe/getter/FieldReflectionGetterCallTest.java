/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.github.spjoe.getter;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class FieldReflectionGetterCallTest {
    private String field1 = "initValue";

    @Test
    void get_hasAccess_shallBeSuccess() throws NoSuchFieldException {
        Field testField = getClass().getDeclaredField("field1");
        FieldReflectionGetterCall<Object, Object> sut = new FieldReflectionGetterCall<>(testField);

        GetterResult<Object> getterResult = sut.get(this);

        assertThat(getterResult.isSuccess(), is(true));
        assertThat(getterResult.orElse("anything"), is("initValue"));
    }

    @Test
    void get_hasNoAccess_shallBeNoSuccess() throws NoSuchFieldException {
        Field testField = getClass().getDeclaredField("field1");
        FieldReflectionGetterCall<Object, Object> sut = new FieldReflectionGetterCall<>(testField);

        testField.setAccessible(false);
        GetterResult<Object> getterResult = sut.get(this);

        assertThat(getterResult.isSuccess(), is(false));
        assertThat(getterResult.orElse("anything"), is("anything"));
    }
}