/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.github.spjoe.getter;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.jupiter.api.Assertions.*;

class GetterResultTest {
    @Test
    void orElse_success_shallReturnResult() {
        GetterResult<String> result = GetterResult.success(null);

        String test = result.orElse("test");

        assertThat(test, nullValue());
    }

    @Test
    void orElseGet_success_shallReturnResult() {
        GetterResult<String> result = GetterResult.success(null);

        String test = result.orElseGet(() -> "test");

        assertThat(test, nullValue());
    }

    @Test
    void orElse_noGetter_shallReturnOrElse() {
        GetterResult<String> result = GetterResult.noGetter();

        String test = result.orElse("test");

        assertThat(test, is("test"));
    }

    @Test
    void orElse_failed_shallReturnOrElse() {
        GetterResult<String> result = GetterResult.failed(new RuntimeException("test exception"));

        String test = result.orElse("test");

        assertThat(test, is("test"));
    }

    @Test
    void orElseGet_noGetter_shallReturnOrElse() {
        GetterResult<String> result = GetterResult.noGetter();

        String test = result.orElseGet(() -> "test");

        assertThat(test, is("test"));
    }

    @Test
    void orElseThrow_success_shallReturnResult() {
        GetterResult<String> result = GetterResult.success("success");

        String test = result.orElseThrow(() -> new RuntimeException("test"));

        assertThat(test, is("success"));
    }

    @Test
    void orElseThrow_noGetter_shallThrow() {
        GetterResult<String> result = GetterResult.noGetter();

        assertThrows(RuntimeException.class, () -> result.orElseThrow(() -> new RuntimeException("test")));
    }

    @Test
    void orElseThrow_failed_shallThrow() {
        GetterResult<String> result = GetterResult.failed(new RuntimeException("test exception"));

        assertThrows(RuntimeException.class, () -> result.orElseThrow(() -> new RuntimeException("test")));
    }

    @Test
    void doesGetterExist_failed_shallReturnTrue() {
        GetterResult<String> result = GetterResult.failed(new RuntimeException("test exception"));

        assertThat(result.doesGetterExist(), is(true));
    }

    @Test
    void doesGetterExist_success_shallReturnTrue() {
        GetterResult<String> result = GetterResult.success("success");

        assertThat(result.doesGetterExist(), is(true));
    }

    @Test
    void doesGetterExist_noGetter_shallReturnFalse() {
        GetterResult<String> result = GetterResult.noGetter();

        assertThat(result.doesGetterExist(), is(false));
    }

    @Test
    void isSuccess_failed_shallReturnFalse() {
        GetterResult<String> result = GetterResult.failed(new RuntimeException("test exception"));

        assertThat(result.isSuccess(), is(false));
    }

    @Test
    void isSuccess_success_shallReturnTrue() {
        GetterResult<String> result = GetterResult.success("success");

        assertThat(result.isSuccess(), is(true));
    }

    @Test
    void isSuccess_noGetter_shallReturnFalse() {
        GetterResult<String> result = GetterResult.noGetter();

        assertThat(result.isSuccess(), is(false));
    }

    @Test
    void get_success_shallReturnResult() {
        GetterResult<String> result = GetterResult.success("success");

        assertThat(result.get(), is("success"));
    }

    @Test
    void isSuccess_noGetter_shallRaiseIllegalStateException() {
        GetterResult<String> result = GetterResult.noGetter();

        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> result.get());
        assertThat(illegalStateException.getMessage(), is("no getter exist to get a result"));

    }

    @Test
    void isSuccess_failed_shallRaiseIllegalStateException() {
        RuntimeException cause = new RuntimeException("test exception");
        GetterResult<String> result = GetterResult.failed(cause);

        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> result.get());
        assertThat(illegalStateException.getCause(), is(cause));
    }
}