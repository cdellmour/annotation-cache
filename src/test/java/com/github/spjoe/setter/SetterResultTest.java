/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.github.spjoe.setter;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

class SetterResultTest {
    @Test
    void doesSetterExist_success_shallReturnTrue() {
        SetterResult result = SetterResult.success();

        boolean actual = result.doesSetterExist();

        assertThat(actual, is(true));
    }

    @Test
    void doesSetterExist_failed_shallReturnTrue() {
        SetterResult result = SetterResult.failed();

        boolean actual = result.doesSetterExist();

        assertThat(actual, is(true));
    }

    @Test
    void doesSetterExist_noSetter_shallReturnFalse() {
        SetterResult result = SetterResult.noSetter();

        boolean actual = result.doesSetterExist();

        assertThat(actual, is(false));
    }

    @Test
    void isSuccess_success_shallReturnTrue() {
        SetterResult result = SetterResult.success();

        boolean actual = result.isSuccess();

        assertThat(actual, is(true));
    }

    @Test
    void isSuccess_failed_shallReturnFalse() {
        SetterResult result = SetterResult.failed();

        boolean actual = result.isSuccess();

        assertThat(actual, is(false));
    }

    @Test
    void isSuccess_noSetter_shallReturnFalse() {
        SetterResult result = SetterResult.noSetter();

        boolean actual = result.isSuccess();

        assertThat(actual, is(false));
    }

    @Test
    void orElseThrow_success_shallReturnResult() {
        SetterResult result = SetterResult.success();

        result.orElseThrow(() -> new RuntimeException("test"));
    }

    @Test
    void orElseThrow_noSetter_shallThrow() {
        SetterResult result = SetterResult.noSetter();

        assertThrows(RuntimeException.class, () -> result.orElseThrow(() -> new RuntimeException("test")));
    }

    @Test
    void orElseThrow_failed_shallThrow() {
        SetterResult result = SetterResult.failed();

        assertThrows(RuntimeException.class, () -> result.orElseThrow(() -> new RuntimeException("test")));
    }
}