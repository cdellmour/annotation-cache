
/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.github.spjoe.setter;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
class MethodSetterCallTest {
    private String field1 = "initValue";

    public String getField1() {
        return field1;
    }

    public void setField1(final String field1) {
        this.field1 = field1;
    }

    @Test
    void set_shallBeSuccess() throws NoSuchMethodException {
        Method testMethod = getClass().getDeclaredMethod("setField1", String.class);
        MethodSetterCall<Object, String> setterCall = new MethodSetterCall<>(testMethod);

        SetterResult setterResult = setterCall.set(this, "test");

        assertThat(setterResult.isSuccess(), is(true));
        assertThat(getField1(), is("test"));
    }

    @Test
    void set_wrongType_shallBeFailure() throws NoSuchMethodException {
        Method testMethod = getClass().getDeclaredMethod("setField1", String.class);
        MethodSetterCall<Object, Object> setterCall = new MethodSetterCall<>(testMethod);

        SetterResult setterResult = setterCall.set(this, 0);

        assertThat(setterResult.isSuccess(), is(false));
        assertThat(getField1(), is("initValue"));
    }
}