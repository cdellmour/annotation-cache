/*
    Copyright 2016 Camillo Dell'mour

    Licensed under the Apache License,Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package com.github.spjoe;

import com.github.spjoe.getter.GetterResult;
import com.github.spjoe.setter.SetterResult;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAndIs;
import static com.github.spjoe.GetterResultMatcher.hasResult;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsSame.sameInstance;

class BeanPropertyAnnotationCacheTest {
    // Annotation tests

    @Test
    void getAnnotationClass_emptyObject_shallBeEmpty() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        Optional<RoutingKey> annotation = sut.getAnnotation(EmptyObject.class);

        assertThat(annotation, isEmpty());
    }

    @Test
    void getAnnotationInstance_emptyObject_shallBeEmpty() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        Optional<RoutingKey> annotation = sut.getAnnotation(new EmptyObject());

        assertThat(annotation, isEmpty());
    }

    @Test
    void getAnnotation_emptyObject_optionalAnnotationShallBeSameInstance() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        Optional<RoutingKey> annotation1 = sut.getAnnotation(new EmptyObject());
        Optional<RoutingKey> annotation2 = sut.getAnnotation(EmptyObject.class);

        assertThat(annotation1, sameInstance(annotation2));
    }

    @Test
    void getAnnotation_objectWithGetterRouting_shallReturnAnnotation() throws NoSuchMethodException {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        Optional<RoutingKey> annotation = sut.getAnnotation(ObjectWithRoutingGetter.class);

        assertThat(annotation, isPresentAndIs(ObjectWithRoutingGetter.class.getDeclaredMethod("getRouting").getAnnotation(RoutingKey.class)));
    }

    @Test
    void getAnnotation_objectWithFieldRouting_shallReturnAnnotation() throws NoSuchFieldException {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        Optional<RoutingKey> annotation = sut.getAnnotation(ObjectWithRoutingField.class);

        assertThat(annotation, isPresentAndIs(ObjectWithRoutingField.class.getDeclaredField("routing").getAnnotation(RoutingKey.class)));
    }

    // GET tests

    @Test
    void get_emptyObject_shallEmptyGetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        GetterResult<Object> objectGetterResult = sut.get(new EmptyObject());

        assertThat(objectGetterResult.doesGetterExist(), is(false));
    }

    @Test
    void get_objectWithRoutingGetter_shallCompleteGetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingGetter objectWithRoutingGetter = new ObjectWithRoutingGetter();
        objectWithRoutingGetter.setRouting("test");
        GetterResult<String> objectGetterResult = sut.get(objectWithRoutingGetter);

        assertThat(objectGetterResult, hasResult("test"));
    }

    @Test
    void get_objectWithRoutingField_shallCompleteGetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingField objectWithRoutingField = new ObjectWithRoutingField();
        objectWithRoutingField.setRouting("test");
        GetterResult<String> objectGetterResult = sut.get(objectWithRoutingField);

        assertThat(objectGetterResult, hasResult("test"));
    }

    @Test
    void get_objectWithRoutingPublicField_shallCompleteGetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingPublicField objectWithRoutingField = new ObjectWithRoutingPublicField();
        objectWithRoutingField.setRouting("test");
        GetterResult<String> objectGetterResult = sut.get(objectWithRoutingField);

        assertThat(objectGetterResult, hasResult("test"));
    }

    @Test
    void get_objectWithRoutingFinalField_shallCompleteGetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingFinalField object = new ObjectWithRoutingFinalField("test");
        GetterResult<String> objectGetterResult = sut.get(object);

        assertThat(objectGetterResult, hasResult("test"));
    }

    // SET tests
    @Test
    void set_emptyObject_shallNoSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        SetterResult setterResult = sut.set(new EmptyObject(), "test");

        assertThat(setterResult.doesSetterExist(), is(false));
    }

    @Test
    void set_objectWithRoutingGetter_shallHasSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingGetter object = new ObjectWithRoutingGetter();
        object.setRouting("test");
        SetterResult setterResult = sut.set(object, "otherValue");

        assertThat(setterResult.doesSetterExist(), is(true));
        assertThat(setterResult.isSuccess(), is(true));
        assertThat(object.getRouting(), is("otherValue"));
    }

    @Test
    void set_objectWithRoutingField_shallHasSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingField object = new ObjectWithRoutingField();
        object.setRouting("test");
        SetterResult setterResult = sut.set(object, "otherValue");

        assertThat(setterResult.doesSetterExist(), is(true));
        assertThat(setterResult.isSuccess(), is(true));
        assertThat(object.getRouting(), is("otherValue"));
    }

    @Test
    void set_objectWithRoutingPublicField_shallHasSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingPublicField object = new ObjectWithRoutingPublicField();
        object.setRouting("test");
        SetterResult setterResult = sut.set(object, "otherValue");

        assertThat(setterResult.doesSetterExist(), is(true));
        assertThat(setterResult.isSuccess(), is(true));
        assertThat(object.getRouting(), is("otherValue"));
    }

    @Test
    void set_objectWithRoutingFinalField_shallHasSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithRoutingFinalField object = new ObjectWithRoutingFinalField("test");
        SetterResult setterResult = sut.set(object, "otherValue");

        assertThat(setterResult.doesSetterExist(), is(true));
        assertThat(setterResult.isSuccess(), is(true));
        assertThat(object.getRouting(), is("otherValue"));
    }

    @Test
    void set_objectWithRoutingBooleanGetter_shallHasSetterResult() {
        BeanPropertyAnnotationCache<Object, RoutingKey> sut = new BeanPropertyAnnotationCache(RoutingKey.class);

        ObjectWithBooleanRouting object = new ObjectWithBooleanRouting();
        SetterResult setterResult = sut.set(object, true);

        assertThat(setterResult.doesSetterExist(), is(true));
        assertThat(setterResult.isSuccess(), is(true));
        assertThat(object.isRouting(), is(true));
    }
}